﻿//
// (c) 2017 Alexandre Aufrere <alexandre@retrust.me>
// Published under the terms of GPL version 3 (as of 29 June 2007)
// see http://www.gnu.org/licenses/gpl.html
//
using System;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CERTENROLLLib;
using System.Runtime.InteropServices;

namespace EnrollmentClient
{
    class CRMPRequest
    {
        private X509Certificate2 clientCert;
        public CRMPRequest()
        {
            clientCert = null;
        }

        public X509Certificate2 selectClientCert()
        {
            X509Certificate2 res = null;

            try
            {
                X509Store x509Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                x509Store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

                X509Certificate2Collection collection = x509Store.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
                X509Certificate2Collection acollection = collection.Find(X509FindType.FindByApplicationPolicy, "1.3.6.1.5.5.7.3.2", false);
                X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(acollection, "Select Certificate", "Select Operator Certificate form the List below.", X509SelectionFlag.SingleSelection);

                if (scollection.Count < 1)
                    return null;
                X509Certificate2Enumerator certEnum = scollection.GetEnumerator();
                certEnum.MoveNext();
                res = certEnum.Current;
           } catch (Exception e)
            {
                throw new Exception("Could not select certificate",e);
            }
            return res;
        }

        public String Initialize()
        {
            String certDN = ConfigurationManager.AppSettings["ClientCertificateDN"];
            X509Store myStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            myStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            foreach (X509Certificate2 curCert in myStore.Certificates)
            {
                if (curCert.SubjectName.Name.ToString().ToLower().Equals(certDN.ToLower()))
                {
                    clientCert = curCert;
                }
            }
            if (clientCert==null)
            {
                throw new ApplicationException("Could not find certificate of DN: " + certDN);
            }
            Dictionary<string, object> version = _makeRequest("action=version");
            var appDescription = version["description"];
            return (String)appDescription;
        }

        public X509Certificate2 enroll (String csr, String cn=null, String mail=null) 
        {
            String certProfile = ConfigurationManager.AppSettings["DefaultCertificateProfile"];
            String defaultMail = ConfigurationManager.AppSettings["DefaultMailAddress"];
            if (mail==null) { mail = defaultMail; }

            // Rebuilding the CSR in PEM format
            StringBuilder p10 = new StringBuilder();
            p10.AppendLine("-----BEGIN CERTIFICATE REQUEST-----");
            p10.AppendLine(Convert.ToBase64String(Convert.FromBase64String(csr), Base64FormattingOptions.InsertLineBreaks));
            p10.AppendLine("-----END CERTIFICATE REQUEST-----");

            // Fetching CN from CSR, just in case
            CX509CertificateRequestPkcs10 pkcs10 = new CX509CertificateRequestPkcs10();
            pkcs10.InitializeDecode(p10.ToString(), EncodingType.XCN_CRYPT_STRING_BASE64REQUESTHEADER);
            String dn = pkcs10.Subject.Name+",dummy";
            dn=Regex.Replace(dn, "cn=([^,]*),.*", "$1", RegexOptions.IgnoreCase);
            if (cn==null) { cn = dn; }

            // now enrolling
            Dictionary<string, object> enrollResponse = _makeRequest("action=enroll&cn=" + Uri.EscapeDataString(cn)
                + "&email=" + Uri.EscapeDataString(mail) + "&profile=" + certProfile + "&pkcs10=" + Uri.EscapeDataString(p10.ToString()));
            String pemCertificate = (String)enrollResponse["certificate"];
            pemCertificate = Regex.Replace(pemCertificate, "-----BEGIN CERTIFICATE-----\r\n", "");
            pemCertificate = Regex.Replace(pemCertificate, "-----END CERTIFICATE-----\r\n", "");
            pemCertificate = Regex.Replace(pemCertificate, "\r\n", "", RegexOptions.Multiline);
            pemCertificate = Regex.Replace(pemCertificate, "\n", "",RegexOptions.Multiline);
            return new X509Certificate2(Encoding.ASCII.GetBytes(pemCertificate));
        }

        public X509Certificate2 enroll_p12(String cn, String mail = null)
        {
            String certProfile = ConfigurationManager.AppSettings["DefaultCertificateProfile"];
            String defaultMail = ConfigurationManager.AppSettings["DefaultMailAddress"];
            if (mail == null) { mail = defaultMail; }

            // now enrolling
            Dictionary<string, object> enrollResponse = _makeRequest("action=enroll&cn=" + Uri.EscapeDataString(cn)
                + "&email=" + Uri.EscapeDataString(mail) + "&profile=" + certProfile);
            String P12B64 = (String)enrollResponse["pkcs12"];
            String P12pwd = (String)enrollResponse["password"];

            return new X509Certificate2(Convert.FromBase64String(P12B64), P12pwd, X509KeyStorageFlags.Exportable);
        }

        public string getAdminSecret()
        {
            Dictionary<string, object> getAdminSecretResponse = _makeRequest("action=scsecret");
            String adminSecret = (String)getAdminSecretResponse["secret"];

            return adminSecret;
        }

        private Dictionary<string, object> _makeRequest(String data)
        {
            //Console.WriteLine(data);
            byte[] encodedData = Encoding.UTF8.GetBytes(data);
            String pkiURL = ConfigurationManager.AppSettings["PkiUrl"];
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(pkiURL);
            wr.ClientCertificates.Add(clientCert);
            wr.Method = "POST";
            wr.ContentType = "application/x-www-form-urlencoded";
            wr.ContentLength = encodedData.Length;
            using (var stream = wr.GetRequestStream())
            {
                stream.Write(encodedData, 0, encodedData.Length);
            }
            var response = (HttpWebResponse)wr.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            //Console.WriteLine(responseString);
            var ser = new JavaScriptSerializer();
            Dictionary<string, object> crmpResponse = ser.DeserializeObject(responseString) as Dictionary<string, object>;
            String status = (String)crmpResponse["status"];
            if (status != "OK")
            {
                throw new ApplicationException("Error performing CRMP Request: " + crmpResponse["msg"]);
            }
            return crmpResponse;
        }
    }
}