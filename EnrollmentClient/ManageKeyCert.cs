﻿//
// (c) 2017 Alexandre Aufrere <alexandre@retrust.me>
// Published under the terms of GPL version 3 (as of 29 June 2007)
// see http://www.gnu.org/licenses/gpl.html
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace EnrollmentClient
{
    class ManageKeyCert
    {
        public enum ProviderType : uint
        {
            PROV_RSA_FULL = 1,
            PROV_RSA_SIG = 2,
            PROV_DSS = 3,
            PROV_FORTEZZA = 4,
            PROV_MS_EXCHANGE = 5,
            PROV_SSL = 6,
            PROV_RSA_SCHANNEL = 12,
            PROV_DSS_DH = 13,
            PROV_EC_ECDSA_SIG = 14,
            PROV_EC_ECNRA_SIG = 15,
            PROV_EC_ECDSA_FULL = 16,
            PROV_EC_ECNRA_FULL = 17,
            PROV_DH_SCHANNEL = 18,
            PROV_SPYRUS_LYNKS = 20,
            PROV_RNG = 21,
            PROV_INTEL_SEC = 22,
            PROV_REPLACE_OWF = 23,
            PROV_RSA_AES = 24,
        }

        [Flags]
        public enum ContextFlags : uint
        {
            CRYPT_VERIFYCONTEXT = 0xF0000000,
            CRYPT_NEWKEYSET = 0x00000008,
            CRYPT_DELETEKEYSET = 0x00000010,
            CRYPT_MACHINE_KEYSET = 0x00000020,
            CRYPT_SILENT = 0x00000040,
            CRYPT_DEFAULT_CONTAINER_OPTIONAL = 0x00000080,
        }

        public enum ProviderParamType : uint
        {
            PP_CLIENT_HWND = 1,
            PP_ENUMALGS = 1,
            PP_ENUMCONTAINERS = 2,
            PP_IMPTYPE = 3,
            PP_NAME = 4,
            PP_VERSION = 5,
            PP_CONTAINER = 6,
            PP_CHANGE_PASSWORD = 7,
            PP_KEYSET_SEC_DESCR = 8,
            PP_CERTCHAIN = 9,
            PP_KEY_TYPE_SUBTYPE = 10,
            PP_CONTEXT_INFO = 11,
            PP_KEYEXCHANGE_KEYSIZE = 12,
            PP_SIGNATURE_KEYSIZE = 13,
            PP_KEYEXCHANGE_ALG = 14,
            PP_SIGNATURE_ALG = 15,
            PP_PROVTYPE = 16,
            PP_KEYSTORAGE = 17,
            PP_APPLI_CERT = 18,
            PP_SYM_KEYSIZE = 19,
            PP_SESSION_KEYSIZE = 20,
            PP_UI_PROMPT = 21,
            PP_ENUMALGS_EX = 22,
            PP_DELETEKEY = 24,
            PP_ENUMMANDROOTS = 25,
            PP_ENUMELECTROOTS = 26,
            PP_KEYSET_TYPE = 27,
            PP_ADMIN_PIN = 31,
            PP_KEYEXCHANGE_PIN = 32,
            PP_SIGNATURE_PIN = 33,
            PP_SIG_KEYSIZE_INC = 34,
            PP_KEYX_KEYSIZE_INC = 35,
            PP_UNIQUE_CONTAINER = 36,
            PP_SGC_INFO = 37,
            PP_USE_HARDWARE_RNG = 38,
            PP_KEYSPEC = 39,
            PP_ENUMEX_SIGNING_PROT = 40,
            PP_CRYPT_COUNT_KEY_USE = 41,
        }

        public enum KeyParameter : uint
        {
            KP_IV = 1,
            KP_SALT = 2,
            KP_PADDING = 3,
            KP_MODE = 4,
            KP_MODE_BITS = 5,
            KP_PERMISSIONS = 6,
            KP_ALGID = 7,
            KP_BLOCKLEN = 8,
            KP_KEYLEN = 9,
            KP_SALT_EX = 10,
            KP_P = 11,
            KP_G = 12,
            KP_Q = 13,
            KP_X = 14,
            KP_Y = 15,
            KP_RA = 16,
            KP_RB = 17,
            KP_INFO = 18,
            KP_EFFECTIVE_KEYLEN = 19,
            KP_SCHANNEL_ALG = 20,
            KP_CLIENT_RANDOM = 21,
            KP_SERVER_RANDOM = 22,
            KP_RP = 23,
            KP_PRECOMP_MD5 = 24,
            KP_PRECOMP_SHA = 25,
            KP_CERTIFICATE = 26,
            KP_CLEAR_KEY = 27,
            KP_PUB_EX_LEN = 28,
            KP_PUB_EX_VAL = 29,
            KP_KEYVAL = 30,
            KP_ADMIN_PIN = 31,
            KP_KEYEXCHANGE_PIN = 32,
            KP_SIGNATURE_PIN = 33,
            KP_PREHASH = 34,
            KP_ROUNDS = 35,
            KP_OAEP_PARAMS = 36,
            KP_CMS_KEY_INFO = 37,
            KP_CMS_DH_KEY_INFO = 38,
            KP_PUB_PARAMS = 39,
            KP_VERIFY_PARAMS = 40,
            KP_HIGHEST_VERSION = 41,
            KP_GET_USE_COUNT = 42,
        }

        const uint PP_KEYEXCHANGE_PIN = 0x20;
        public const uint CRYPT_FIRST = 0x00000001;
        public const uint CRYPT_NEXT = 0x00000002;
        public const uint ERROR_NO_MORE_ITEMS = 0x00000103;
        public const string szMSBaseCsp = "Microsoft Base Smart Card Crypto Provider";

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Boolean CryptAcquireContext(
            out IntPtr hProv,
            string pszContainer,
            string pszProvider,
            ProviderType dwProvType,
            ContextFlags dwFlags);

        [DllImport("advapi32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern Boolean CryptGetProvParam(
             IntPtr hProv,
             ProviderParamType dwParam,
             StringBuilder pbData,
             ref UInt32 pdwDataLen,
             UInt32 dwFlags);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Boolean CryptImportKey(
             IntPtr hProv,
             byte[] pbKeyData,
             UInt32 dwDataLen,
             IntPtr hPubKey,
             UInt32 dwFlags,
             ref IntPtr hKey);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Boolean CryptSetKeyParam(
             IntPtr hKey,
             KeyParameter dwParam,
             byte[] pbData,
             UInt32 dwFlags);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Boolean CryptDestroyKey(
             IntPtr phKey);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Boolean CryptReleaseContext(
             IntPtr hProv,
             UInt32 dwFlags);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Boolean CryptSetProvParam(IntPtr hProv, 
            uint dwParam, 
            [In] byte[] pbData, 
            uint dwFlags);

        [DllImport("kernel32.dll")]
        static extern uint GetLastError();

        public static Boolean ImportX509CertificateWithKeyIntoSmartCard(X509Certificate2 cert, string pin)
        {
            if (!cert.HasPrivateKey)
                throw new Exception("No private key to import");
            //We get key and cert from the X509Certificate2 object
            RSACryptoServiceProvider key = (RSACryptoServiceProvider)cert.PrivateKey;
            byte[] pBlob = key.ExportCspBlob(true);
            byte[] pbCert = cert.RawData;

            IntPtr hProv = IntPtr.Zero;

            //First we play the PIN
            bool ret = CryptAcquireContext(out hProv, null, szMSBaseCsp, ProviderType.PROV_RSA_FULL, ContextFlags.CRYPT_DEFAULT_CONTAINER_OPTIONAL);
            ret = CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, Encoding.ASCII.GetBytes(pin), 0);
            if (!ret) return false;

            //Then we create the new container
            ret = CryptAcquireContext(out hProv, "", szMSBaseCsp, ProviderType.PROV_RSA_FULL, ContextFlags.CRYPT_NEWKEYSET);
            if (ret)
            {
                //Now we import the key
                IntPtr hKey = IntPtr.Zero;
                ret = CryptImportKey(hProv, pBlob, (UInt32)pBlob.Length, IntPtr.Zero, 0, ref hKey);
                if (ret)
                {
                    //And finally we import the certificate
                    ret= CryptSetKeyParam(hKey, KeyParameter.KP_CERTIFICATE, pbCert, 0);
                    if (!ret)
                    {
                        CryptDestroyKey(hKey);
                        return false;
                    }
                    CryptDestroyKey(hKey);
                    return true;
                }
            }
            return false;
        }

        public static Boolean WipeCard(string pin)
        {
            X509Store x509Store = null;

            try
            {
                x509Store = new X509Store(StoreName.My);
                x509Store.Open(OpenFlags.ReadWrite | OpenFlags.OpenExistingOnly);

                List<string> containers = GetKeyContainers(pin);

                // In case the card is empty, let's tell about
                if (containers.ToArray().Length == 0) return false;

                // Loop through containers to delete contents
                foreach (string container in containers)
                {
                    deleteCertificate(container, pin);
                }
            } catch (Exception e)
            {
                throw new Exception("Error wiping card", e);
            }
            return true;
        }

        private static void deleteCertificate(string containerName, string pin)
        {

            IntPtr hProv = IntPtr.Zero;

            try
            {
                //First we play the PIN
                bool ret = CryptAcquireContext(out hProv, null, szMSBaseCsp, ProviderType.PROV_RSA_FULL, ContextFlags.CRYPT_DEFAULT_CONTAINER_OPTIONAL);
                ret = CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, Encoding.ASCII.GetBytes(pin), 0);
                if (!ret) throw new Win32Exception(Marshal.GetLastWin32Error());

                // Then we delete the keys...
                if (!CryptAcquireContext(out hProv, containerName, szMSBaseCsp, ProviderType.PROV_RSA_FULL, ContextFlags.CRYPT_DELETEKEYSET))
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                if (hProv != IntPtr.Zero)
                {
                    if (!CryptReleaseContext(hProv, 0))
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    hProv = IntPtr.Zero;
                }
            }
            catch (Exception e)
            {
                if (hProv != IntPtr.Zero)
                {
                    if (!CryptReleaseContext(hProv, 0))
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    hProv = IntPtr.Zero;
                }

                throw new Exception("Error deleting containers", e);
            }
        }

        private static List<string> GetKeyContainers(string pin)
        {
            List<string> containers = new List<string>();

            IntPtr hProv = IntPtr.Zero;

            try
            {
                //First we play the PIN
                bool ret = CryptAcquireContext(out hProv, null, szMSBaseCsp, ProviderType.PROV_RSA_FULL, ContextFlags.CRYPT_DEFAULT_CONTAINER_OPTIONAL);
                ret = CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, Encoding.ASCII.GetBytes(pin), 0);
                if (!ret) throw new Win32Exception(Marshal.GetLastWin32Error());

                //We set the context to read the contents
                if (!CryptAcquireContext(out hProv, null, szMSBaseCsp, ProviderType.PROV_RSA_FULL, ContextFlags.CRYPT_VERIFYCONTEXT))
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                uint pcbData = 0;
                uint dwFlags = CRYPT_FIRST;
                //We enumerate the containers
                if (!CryptGetProvParam(hProv, ProviderParamType.PP_ENUMCONTAINERS, null, ref pcbData, dwFlags))
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                StringBuilder sb = new StringBuilder((int)pcbData + 1);
                //And loop through them
                while (CryptGetProvParam(hProv, ProviderParamType.PP_ENUMCONTAINERS, sb, ref pcbData, dwFlags))
                {
                    containers.Add(sb.ToString());
                    dwFlags = CRYPT_NEXT;
                }

                //We check that we've finished loopiong correctly
                int err = Marshal.GetLastWin32Error();
                if (err != ERROR_NO_MORE_ITEMS)
                    throw new Win32Exception(err);

                if (hProv != IntPtr.Zero)
                {
                    if (!CryptReleaseContext(hProv, 0))
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    hProv = IntPtr.Zero;
                }
            }
            catch (Exception e)
            {
                if (hProv != IntPtr.Zero)
                {
                    if (!CryptReleaseContext(hProv, 0))
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    hProv = IntPtr.Zero;
                }

                throw new Exception("Error listing containers",e);
            }

            return containers;
        }
    }
}
