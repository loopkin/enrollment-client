﻿//
// (c) 2017 Alexandre Aufrere <alexandre@retrust.me>
// Published under the terms of GPL version 3 (as of 29 June 2007)
// see http://www.gnu.org/licenses/gpl.html
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EnrollmentClient
{
    class Program
    {
       // Utility method
       private static string[] MultiStringToArray(byte[] multistring)
        {
            List<string> stringList = new List<string>();
            int i = 0;
            while (i < multistring.Length)
            {
                int j = i;
                if (multistring[j++] == '\0') break;
                while (j < multistring.Length)
                {
                    if (multistring[j++] == '\0')
                    {
                        stringList.Add(new string(System.Text.Encoding.ASCII.GetString(multistring,0,multistring.Length).ToCharArray(), i, j - i - 1));
                        i = j;
                        break;
                    }
                }
            }

            return stringList.ToArray();
        }

        // Main exe
        static void Main(string[] args)
        {
            if (args.Length<1)
            {
                System.Console.Out.Write("Usage: EnrollmentClient.exe selectCert | format | PIN CommonName E-Mail\n");
                return;
            }

            string pin = args[0];
            string cn = "";
            string mail = "";

            Regex pinPolicy = new Regex("^[0-9]+$");
            if ((!pin.Equals("selectCert")) && (!pin.Equals("format")) && (!pinPolicy.IsMatch(pin)) )
            {
                System.Console.Out.Write("PIN must contain only numbers\n");
                return;
            }

            if ((!pin.Equals("selectCert")) && (!pin.Equals("format")))
            {
                if (args.Length < 3)
                {
                    System.Console.Out.Write("Usage: EnrollmentClient.exe format | PIN CommonName E-Mail\n");
                    return;
                }
                cn = args[1];
                mail = args[2];
            }

            CRMPRequest crmp = new CRMPRequest();
            // first we manage the cert selection option, which is purely local
            if (pin.Equals("selectCert")) {
                try
                {
                    X509Certificate2 adminCert = crmp.selectClientCert();
                    if (adminCert != null)
                        System.Console.Out.Write(adminCert.SubjectName.Name.ToString());
                } catch (Exception e)
                {
                    System.Console.Out.Write("Error selecting cert " + e);
                }
                return;
            }

            long ret = 0;
            int hContext = 0;
            int pcchReaders = 0;

            // First we get the list of readers

            //establish context
            ret = ModWinsCard.SCardEstablishContext(ModWinsCard.SCARD_SCOPE_USER, 0, 0, ref hContext);
            //get readers buffer len
            ret = ModWinsCard.SCardListReaders(hContext, null, null, ref pcchReaders);
            byte[] mszReaders = new byte[pcchReaders];
            //System.Console.Out.Write("Number of readers:" + pcchReaders);
            // fill readers' buffer
            ret = ModWinsCard.SCardListReaders(hContext, null, mszReaders, ref pcchReaders);
            string[] readersList = MultiStringToArray(mszReaders);
            if (readersList.ToArray().Length == 0)
            {
                System.Console.Out.Write(ModWinsCard.GetScardErrMsg(ModWinsCard.SCARD_E_NO_READERS_AVAILABLE));
                return;
            }

            // Then we iterate through the readers
            foreach (string item in readersList.ToArray())
            {
                System.Console.Out.Write("Reader: "+item+"\n");
                
                // We try to connect to the card in the reader, to verify that there's a chance we can enroll it then
                int Protocol = 0;
                int hCard = 0;
                int retCode = ModWinsCard.SCardConnect(hContext, item, ModWinsCard.SCARD_SHARE_EXCLUSIVE,
                          ModWinsCard.SCARD_PROTOCOL_T0 | ModWinsCard.SCARD_PROTOCOL_T1, ref hCard, ref Protocol);
                if (retCode != ModWinsCard.SCARD_S_SUCCESS)
                {
                    System.Console.Out.Write("Could not connect to smart card");
                    continue;
                }
                ret = ModWinsCard.SCardReleaseContext(hContext);

                // Initializing connection to the PKI
                string pkiDesc = null;
                try
                {
                    pkiDesc = crmp.Initialize();
                }
                catch (Exception e)
                {
                    System.Console.Out.Write("Could not connect to PKI:\n" + e.ToString());
                    return;
                }
                System.Console.Out.Write("Connecting to PKI: " + pkiDesc + "\n");

                // Fetching admin secret from the PKI
                string newAdminSecret = null;
                try
                {
                    newAdminSecret = crmp.getAdminSecret();
                }
                catch (Exception e)
                {
                    System.Console.Out.Write("Could not get admin secret from PKI:\n" + e.ToString());
                    return;
                }

                // We create the PIN Manager, checking if the admin secret is already the one provided
                PinAndAdminSecretManager pinManager = new PinAndAdminSecretManager(newAdminSecret);

                // We change anyway the admin secret to the one provided by the PKI
                bool result = pinManager.changeAdminSecret(newAdminSecret);
                if (result)
                    System.Console.Out.Write("Successfully set administration secret\n");
                else
                {
                    System.Console.Out.Write("Failure setting administration secret\n");
                    return;
                }

                // if we just need to wipe, then wipe
                if (pin.Equals("format"))
                {
                    try
                    {
                        result=ManageKeyCert.WipeCard(PinAndAdminSecretManager.defaultPIN);
                        if (!result)
                            System.Console.Out.Write("Card was blank already");
                        else
                            System.Console.Out.Write("Card is now empty.");
                    } catch (Exception e)
                    {
                        System.Console.Out.Write("Unable to format card" + e);
                    }
                    return;
                }

                // Then we change the PIN
                result = pinManager.unblockAndSetPin(pin);
                if (result)
                    System.Console.Out.Write("Successfully set PIN\n");
                else
                {
                    System.Console.Out.Write("Failure setting PIN\n");
                    return;
                }

                // We fetch the certificate from the PKI
                X509Certificate2 cert = null;
                try
                {
                    cert = crmp.enroll_p12(cn, mail);
                } catch (Exception e)
                {
                    System.Console.Out.Write("Could not retrieve certificate from PKI:\n" + e.ToString());
                    return;                
                }
                System.Console.Out.Write("Successfully retrieved certificate from PKI\n");

                // Finally we import the certificate into the card
                result = ManageKeyCert.ImportX509CertificateWithKeyIntoSmartCard(cert, pin);
                if (result)
                    System.Console.Out.Write("Successfully Enrolled "+cn);
                else
                    System.Console.Out.Write("Failure importing Certificate on card");

                // In practice we can work only with one reader for now
                break;
            }
        }
    }
}
