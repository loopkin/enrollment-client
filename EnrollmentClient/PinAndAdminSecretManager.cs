﻿//
// (c) 2017 Alexandre Aufrere <alexandre@retrust.me>
// Published under the terms of GPL version 3 (as of 29 June 2007)
// see http://www.gnu.org/licenses/gpl.html
//
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnrollmentClient
{
    class PinAndAdminSecretManager
    {

        private string adminSecret = "";
        public const string defaultPIN = "9753287187";

        public PinAndAdminSecretManager(string SCAdministrationSecret=null) {
            adminSecret = SCAdministrationSecret;
            // If no admin secret was set, let's try the default one: 48 zeros is default one for most cards (Gemalto MD, Oberthur AuthentIC)
            if (adminSecret == null)
                adminSecret = "000000000000000000000000000000000000000000000000";
            else
            {
                // Else we test if we can unblock the card using given admin secret. If not, we'll set the default admin secret.
                if (unblockAndSetPin(defaultPIN))
                    return;
                else
                    adminSecret = "000000000000000000000000000000000000000000000000";
            }
            // Let's try the 48 zeros for a PIN change
            // If it fails, we are probably using an eToken. Default admin secret is computed from default SOPIN (1234567890)
            if (unblockAndSetPin(defaultPIN))
            {
                System.Console.Out.Write("Detected Standard Minidriver Card (Gemalto or Oberthur)\n");
            }
            else
            {
                adminSecret = "1D6A4F7A652E18203E3D3B0C70451022107F7420216E611B";
                System.Console.Out.Write("Detected Gemalto-Safenet Minidriver Card (eToken)\n");
            }
        }

        private string executeSCUtil(string command, string[] args)
        {
            //Create process
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            String installationPath = ConfigurationManager.AppSettings["InstallationPath"];

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = installationPath+"\\scUtil.exe";

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = command + " " + args[0] + " " + args[1];
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;

            //Start the process
            pProcess.Start();

            //Get program output
            string strOutput = pProcess.StandardOutput.ReadToEnd();

            //Wait for process to finish
            pProcess.WaitForExit();

            return strOutput;
        }

        public bool unblockAndSetPin(string pin)
        {
            string ret = executeSCUtil("unblockpin", new string[2] { adminSecret, pin } );

            if (ret.Equals("Success!"))
                return true;

            return false;
        }

        public bool changeAdminSecret(string newAdminSecret)
        {
            if (adminSecret.Equals(newAdminSecret))
                return true;

            string ret = executeSCUtil("changeadminkey", new string[2] { adminSecret, newAdminSecret });

            if (ret.Equals("Success!")) {
                adminSecret = newAdminSecret;
                return true;
            }

            return false;
        }
    }
}
