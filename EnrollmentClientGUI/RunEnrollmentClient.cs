﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnrollmentClientGUI
{
    class RunEnrollmentClient
    {
        public static string RunEnroll(string pin, string cn, string mail)
        {
            //Create process
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            String installationPath = ConfigurationManager.AppSettings["InstallationPath"];

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = installationPath + "\\EnrollmentClient.exe";

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = pin + " \"" + cn + "\" " + mail;
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;

            //Start the process
            pProcess.Start();

            //Get program output
            string strOutput = pProcess.StandardOutput.ReadToEnd();

            //Wait for process to finish
            pProcess.WaitForExit();

            return strOutput;
        }

        public static string RunFormat()
        {
            //Create process
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            String installationPath = ConfigurationManager.AppSettings["InstallationPath"];

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = installationPath + "\\EnrollmentClient.exe";

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = "format";
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;

            //Start the process
            pProcess.Start();

            //Get program output
            string strOutput = pProcess.StandardOutput.ReadToEnd();

            //Wait for process to finish
            pProcess.WaitForExit();

            return strOutput;
        }
    }
}
