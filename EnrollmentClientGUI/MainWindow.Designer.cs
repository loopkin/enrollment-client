﻿namespace EnrollmentClientGUI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.pinBox = new System.Windows.Forms.TextBox();
            this.CNBox = new System.Windows.Forms.TextBox();
            this.MailBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.EnrollButton = new System.Windows.Forms.Button();
            this.EnrollmentWait = new System.Windows.Forms.Label();
            this.FormatButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pinBox
            // 
            this.pinBox.Location = new System.Drawing.Point(83, 76);
            this.pinBox.Name = "pinBox";
            this.pinBox.Size = new System.Drawing.Size(168, 20);
            this.pinBox.TabIndex = 0;
            // 
            // CNBox
            // 
            this.CNBox.Location = new System.Drawing.Point(83, 117);
            this.CNBox.Name = "CNBox";
            this.CNBox.Size = new System.Drawing.Size(167, 20);
            this.CNBox.TabIndex = 1;
            // 
            // MailBox
            // 
            this.MailBox.Location = new System.Drawing.Point(83, 157);
            this.MailBox.Name = "MailBox";
            this.MailBox.Size = new System.Drawing.Size(168, 20);
            this.MailBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "PIN:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "CN:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mail/UPN:";
            // 
            // EnrollButton
            // 
            this.EnrollButton.Location = new System.Drawing.Point(103, 227);
            this.EnrollButton.Name = "EnrollButton";
            this.EnrollButton.Size = new System.Drawing.Size(75, 23);
            this.EnrollButton.TabIndex = 6;
            this.EnrollButton.Text = "Enroll";
            this.EnrollButton.UseVisualStyleBackColor = true;
            this.EnrollButton.Click += new System.EventHandler(this.EnrollButtonClick);
            // 
            // EnrollmentWait
            // 
            this.EnrollmentWait.AutoSize = true;
            this.EnrollmentWait.Location = new System.Drawing.Point(57, 201);
            this.EnrollmentWait.Name = "EnrollmentWait";
            this.EnrollmentWait.Size = new System.Drawing.Size(183, 13);
            this.EnrollmentWait.TabIndex = 7;
            this.EnrollmentWait.Text = "Enrollment in Progress, Please Wait...";
            this.EnrollmentWait.Visible = false;
            // 
            // FormatButton
            // 
            this.FormatButton.Location = new System.Drawing.Point(103, 27);
            this.FormatButton.Name = "FormatButton";
            this.FormatButton.Size = new System.Drawing.Size(75, 23);
            this.FormatButton.TabIndex = 8;
            this.FormatButton.Text = "Format";
            this.FormatButton.UseVisualStyleBackColor = true;
            this.FormatButton.Click += new System.EventHandler(this.FormatButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.FormatButton);
            this.Controls.Add(this.EnrollmentWait);
            this.Controls.Add(this.EnrollButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MailBox);
            this.Controls.Add(this.CNBox);
            this.Controls.Add(this.pinBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enrollment Client GUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox pinBox;
        private System.Windows.Forms.TextBox CNBox;
        private System.Windows.Forms.TextBox MailBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button EnrollButton;
        private System.Windows.Forms.Label EnrollmentWait;
        private System.Windows.Forms.Button FormatButton;
    }
}

