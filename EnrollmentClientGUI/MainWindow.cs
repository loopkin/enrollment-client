﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EnrollmentClientGUI
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void EnrollButtonClick(object sender, EventArgs e)
        {
            EnrollButton.Visible = false;
            EnrollmentWait.Visible = true;
            FormatButton.Visible = false;
            string pin = pinBox.Text;
            pinBox.Text = "";
            this.Refresh();
            string res = RunEnrollmentClient.RunEnroll(pin, CNBox.Text, MailBox.Text);
            MessageBox.Show(res,"Enrollment Results");
            EnrollButton.Visible = true;
            EnrollmentWait.Visible = false;
            FormatButton.Visible = true;
            this.Refresh();
        }

        private void FormatButton_Click(object sender, EventArgs e)
        {
            DialogResult dr=MessageBox.Show("Are you sure you want to format the inserted Smart Card?", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.No)
                return;
            EnrollButton.Visible = false;
            EnrollmentWait.Visible = false;
            FormatButton.Visible = false;
            this.Refresh();
            string res = RunEnrollmentClient.RunFormat();
            MessageBox.Show(res, "Formatting Results");
            EnrollButton.Visible = true;
            EnrollmentWait.Visible = false;
            FormatButton.Visible = true;
            this.Refresh();
        }
    }
}
